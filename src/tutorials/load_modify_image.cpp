#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char **argv)
{
    // Read the filename
    char *imageName = argv[1];

    // load image from file
    Mat image = imread(imageName, IMREAD_COLOR);

    if (argc != 2 || image.empty())
    { // verify the image was correctly loaded
        cerr << " No image data " << endl;
        return EXIT_FAILURE;
    }

    // create new Mat, for the grey image
    Mat gray_image;

    // convert the loaded image to grey, and copy it to gray_image
    cvtColor(image, gray_image, cv::COLOR_BGR2GRAY);

    // write greyscale image to file
    imwrite("Gray_Image.jpg", gray_image);

    // create windows
    namedWindow(imageName, cv::WINDOW_AUTOSIZE);
    namedWindow("Gray image", cv::WINDOW_AUTOSIZE);

    // show on windows
    imshow(imageName, image);
    imshow("Gray image", gray_image);

    // Wait for a keystroke in the window
    waitKey(0);

    return EXIT_SUCCESS;
}
