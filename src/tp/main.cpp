#include "ocv_utils.hpp"

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <iostream>
#include <omp.h>

using namespace cv;
using namespace std;

// ------------------------------------------------------------------------------------------------------------------------

void printHelp(const string &progName)
{
    cout << "Usage:\n\t " << progName << " <image_file> <K_num_of_clusters> [<image_ground_truth>]" << endl;
}

int min(int a, int b)
{
    return a < b ? a : b;
}

int max(int a, int b)
{
    return a > b ? a : b;
}

/**
 * @brief Calculate the euclidean distance between {r,it_max,b} and {R,G,B}
 *
 * @param r red
 * @param g green
 * @param b blue
 * @param R Red
 * @param G Green
 * @param B Blue
 * @return distance between {r,it_max,b} and {R,G,B}
 */
int distance(int r, int g, int b, int R, int G, int B)
{
    return sqrt((r - R) * (r - R) + (g - G) * (g - G) + (b - B) * (b - B));
}

/**
 * @brief Calculate the index of the closest center from the color
 *
 * @param color
 * @param centers
 * @return index of the closest center
 */
int closestCenter(Vec3b color, Mat centers)
{
    // Extract r it_max b values
    int r = color[0];
    int it_max = color[1];
    int b = color[2];

    // Initialize variables
    int minDistance = sqrt(3 * 255 * 255) + 1;
    int closest;

    // Test each centers
    for (int i = 0; i < centers.rows; i++)
    {
        // Extract R G B values from the current center
        int R = centers.at<float>(i, 0);
        int G = centers.at<float>(i, 1);
        int B = centers.at<float>(i, 2);

        // Replace varibales with new values if necessary
        int curDistance = distance(r, it_max, b, R, G, B);
        if (curDistance < minDistance)
        {
            minDistance = curDistance;
            closest = i;
        }
    }
    return closest;
}

// ------------------------------------------------------------------------------------------------------------------------

/**
 * @brief Calculate the k-means of the vect
 *
 * @param vect
 * @param k
 * @return Centers of vect
 */
Mat kmeans(Mat vect, int k)
{
    Mat centers(k, 1, CV_32FC3);

    // initialisation des centres
    for (int i = 0; i < k; i++)
    {
        centers.at<Vec3f>(i) = vect.at<Vec3f>(i * vect.rows / k);
    }

    int it = 0;
    int it_max = 20000;
    int counters[k];
    Mat prevCenters;

    do
    {
        // initialiser les varibales de boucle
        prevCenters = centers.clone();
        for (int i = 0; i < k; i++)
        {
            counters[i] = 0;
        }

        // compter les centres
        // sommer les pixels de chaque classe
        centers = Mat::zeros(k, 1, CV_32FC3);
        for (int i = 0; i < vect.cols; i++)
        {
            Vec3f color = vect.at<Vec3f>(i);
            int closest = closestCenter(color, prevCenters);
            centers.at<Vec3f>(closest) += color;
            counters[closest]++;
        }

        // faire la moyennes des pixels de chaque classe
        for (int i = 0; i < k; i++)
        {
            if (counters[i] != 0)
                centers.at<Vec3f>(i) /= counters[i];
        }

    } while (it++ < it_max && sum(centers != prevCenters) != Scalar(0, 0, 0, 0));
    cout << "kmeans terminé en " << it << " itérations." << endl << endl;

    return centers;
}

// ------------------------------------------------------------------------------------------------------------------------

/**
 * @brief 
 * 
 * @param s_i 
 * @param s_j 
 * @param d_i 
 * @param d_j 
 * @param matIMG 
 * @param matMOY 
 * @param matCOUNT 
 * @param hc 
 * @return float 
 */
float compute(int s_i, int s_j, int d_i, int d_j, Mat *matIMG, Mat *matMOY, Mat *matCOUNT, double hc)
{
    Vec3f centre = matIMG->at<Vec3f>(s_i, s_j);
    Vec3f dest = matIMG->at<Vec3f>(d_i, d_j);

    // seuil chromatique
    float dist = cv::norm(dest - centre);
    if (dist < hc)
    {
        matMOY->at<Vec3f>(s_i, s_j) += dest;
        matMOY->at<Vec3f>(d_i, d_j) += centre;

        matCOUNT->at<float>(s_i, s_j)++;
        matCOUNT->at<float>(d_i, d_j)++;

        return dist;
    }

    return 0;
}

/**
 * @brief Calculate the k-means of the matIMG
 * 
 * @param hs 
 * @param hc 
 * @param eps 
 * @param itMax 
 * @param matIMG 
 * @return Mat 
 */
Mat meanshift(uint hs, double hc, double eps, uint itMax, Mat matIMG)
{
    Mat matMOY, matCOUNT;
    float maxDist, dist;
    int i, j;

    int it = 0;

    #pragma omp parallel shared(it)

    do {
        #pragma omp single
        {
        cout << " mean-shit (" << it << "/" << itMax << ")" << endl;

        matMOY = Mat::zeros(matIMG.rows, matIMG.cols, CV_32FC3);
        matCOUNT = Mat::zeros(matIMG.rows, matIMG.cols, CV_32FC1);
        maxDist = 0;
        it++;
        }

        #pragma omp for private(i, j)
        for (i = 0; i < matIMG.rows; i++)
        {
            for (j = 0; j < matIMG.cols; j++)
            {
                // pixels à droite du centre
                for (int j2 = j; j2 < min(j + hs + 1, matIMG.cols); j2++)
                {
                    dist = compute(i, j, i, j2, &matIMG, &matMOY, &matCOUNT, hc);
                    if (dist > maxDist)
                    {
                        maxDist = dist;
                    }
                }

                // pixels en dessous du centre
                for (int i2 = i + 1; i2 < min(i + hs + 1, matIMG.rows); i2++)
                {
                    for (int j2 = max(j - hs, 0); j2 < min(j + hs + 1, matIMG.cols); j2++)
                    {
                        dist = compute(i, j, i2, j2, &matIMG, &matMOY, &matCOUNT, hc);
                        if (dist > maxDist)
                        {
                            maxDist = dist;
                        }
                    }
                }

                matIMG.at<Vec3f>(i, j) = matMOY.at<Vec3f>(i, j) / matCOUNT.at<float>(i, j);
            }
        }
        // cout << "maxDist = " << maxDist << endl;
    }
    while (it < itMax);
    
    return matIMG;
}

// ------------------------------------------------------------------------------------------------------------------------

/**
 * @brief 
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char **argv)
{
    if (argc != 3 && argc != 4)
    {
        cout << " Incorrect number of arguments." << endl;
        printHelp(string(argv[0]));
        return EXIT_FAILURE;
    }

    const auto imageFilename = string(argv[1]);
    const string groundTruthFilename = (argc == 4) ? string(argv[3]) : string();
    const int k = stoi(argv[2]);

    if (k < 1)
    {
        cout << " k must be a positive integer" << endl;
        printHelp(string(argv[0]));
        return EXIT_FAILURE;
    }

    // trust file used ?
    bool useTrust = !groundTruthFilename.empty() && k == 2;

    // just for debugging
    {
        cout << endl
             << " Program called with the following arguments:" << endl;
        cout << " \timage file: " << imageFilename << endl;
        cout << " \tk: " << k << endl;
        if (useTrust)
            cout << " \tground truth segmentation: " << groundTruthFilename << endl
                 << endl;
    }

    // load the color image to process from file
    Mat sourceMatrix = imread(imageFilename, IMREAD_COLOR);
    Mat trustMatrix = imread(groundTruthFilename, IMREAD_COLOR);

    // for debugging use the macro PRINT_MAT_INFO to print the info about the matrix, like size and type
    PRINT_MAT_INFO(sourceMatrix);

    // convert the image into floats (CV_32F), for kmeans
    Mat sourceMatrix32f;
    sourceMatrix.convertTo(sourceMatrix32f, CV_32F);

    // reshape the image into a 1D array, for kmeans
    Mat sourceMatrixReshaped = sourceMatrix32f.reshape(3, 1);

    // Call OpenCV's kmeans function
    Mat labels, centers;
    kmeans(
        sourceMatrixReshaped,
        k,
        labels,
        TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 10, 1.0),
        3,
        KMEANS_PP_CENTERS,
        centers);

    // Call ou kmeans function
    centers = kmeans(sourceMatrixReshaped, k);

    // Create result matrix
    Mat resultMatrix = sourceMatrix.clone();
    Mat diffMatrix = trustMatrix.clone();

    // Some usefull colors
    Vec3b white(255, 255, 255);
    Vec3b black(0, 0, 0);
    Vec3b red(255, 0, 0);
    Vec3b blue(0, 0, 255);

    Vec3b colorMaj, colorMin;
    if (useTrust)
    {
        // Count the number of pixel per class in the trust matrix
        int n = 0, p = 0;
        for (int i = 0; i < trustMatrix.rows; i++)
        {
            for (int j = 0; j < trustMatrix.cols; j++)
            {
                if (trustMatrix.at<Vec3b>(i, j) == white)
                {
                    n++;
                }
                else
                {
                    p++;
                }
            }
        }

        // Assign black/white to colorMaj/colorMin
        if (n > p)
        {
            colorMaj = white;
            colorMin = black;
        }
        else
        {
            colorMaj = black;
            colorMin = white;
        }

        // Count the number of pixel per class in the source matrix
        n = 0;
        p = 0;
        for (int i = 0; i < resultMatrix.rows; i++)
        {
            for (int j = 0; j < resultMatrix.cols; j++)
            {
                Vec3b color = sourceMatrix.at<Vec3b>(i, j);
                int closest = closestCenter(color, centers);
                if (closest == 0)
                {
                    n++;
                }
                else
                {
                    p++;
                }
            }
        }

        // Reorganize centers to match with the trust matrix
        if (n < p)
        {
            Vec3f tmp = centers.at<Vec3f>(0);
            centers.at<Vec3f>(0) = centers.at<Vec3f>(1);
            centers.at<Vec3f>(1) = tmp;
        }
    }

    // Quality counters
    float TP = 0;
    float FP = 0;
    float TN = 0;
    float FN = 0;

    // Loop for each pixel
    for (int i = 0; i < resultMatrix.rows; i++)
    {
        for (int j = 0; j < resultMatrix.cols; j++)
        {
            Vec3b color = sourceMatrix.at<Vec3b>(i, j);

            int closest = closestCenter(color, centers);

            // Set pixel color to the color of the closest center
            // (Or black/white if we are using a trust image)
            if (useTrust && closest == 0)
            {
                resultMatrix.at<Vec3b>(i, j) = colorMaj;
            }
            else if (useTrust && closest == 1)
            {
                resultMatrix.at<Vec3b>(i, j) = colorMin;
            }
            else
            {
                resultMatrix.at<Vec3b>(i, j) = centers.at<Vec3b>(closest);
            }

            // Increment counters for the quality evaluations
            if (useTrust)
            {
                if (trustMatrix.at<Vec3b>(i, j) != resultMatrix.at<Vec3b>(i, j))
                {
                    diffMatrix.at<Vec3b>(i, j) = trustMatrix.at<Vec3b>(i, j) == colorMaj ? red : blue;

                    if (trustMatrix.at<Vec3b>(i, j) == colorMaj)
                    {
                        // False negative
                        FN++;
                    }
                    else
                    {
                        // False positive
                        FP++;
                    }
                }
                else
                {
                    if (trustMatrix.at<Vec3b>(i, j) == colorMaj)
                    {
                        // True positive
                        TP++;
                    }
                    else
                    {
                        // True  negative
                        TN++;
                    }
                }
            }
        }
    }

    // Trust comparison results
    if (useTrust)
    {
        float P = TP / (TP + FP);
        float S = TP / (TP + FN);
        float DSC = 2 * TP / (2 * TP + FP + FN);

        cout << endl
             << "counters = " << endl
             << " TP = " << TP << endl
             << " TN = " << TN << endl
             << " FP = " << FP << endl
             << " FN = " << FN << endl
             << " Total (debug) = " << (TP + TN + FP + FN) << endl
             << endl
             << " Precision = " << P << endl
             << " Sensibility = " << S << endl
             << " DICE Similarity Coefficient = " << DSC << endl;
    }

    // compute meanshift for the image
    int hs = 2;
    int hc = 10;
    double eps = 1.0;
    int ite = 5;

    Mat msMatrix = meanshift(hs, hc, eps, ite, sourceMatrix32f);
    Mat msMatrixU;
    msMatrix.convertTo(msMatrixU, CV_8U);

    // create image windows
    namedWindow("Source", cv::WINDOW_AUTOSIZE);
    namedWindow("Result", cv::WINDOW_AUTOSIZE);
    if (useTrust)
    {
        namedWindow("Trust", cv::WINDOW_AUTOSIZE);
        namedWindow("Diff", cv::WINDOW_AUTOSIZE);
    }
    namedWindow("Mean Shift", cv::WINDOW_AUTOSIZE);

    // show images
    imshow("Source", sourceMatrix);
    imshow("Result", resultMatrix);
    if (useTrust)
    {
        imshow("Trust", trustMatrix);
        imshow("Diff", diffMatrix);
    }
    imshow("Mean Shift", msMatrixU);

    // press q to end
    while (waitKey(0) != 113);

    return EXIT_SUCCESS;
}
